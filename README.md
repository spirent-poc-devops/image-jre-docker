# <img src="https://upload.wikimedia.org/wikipedia/en/thumb/c/ce/Spirent_logo.svg/490px-Spirent_logo.svg.png" alt="Spirent Logo" width="200"> <br/> Java Runtime Base Image

The base image for Java runtime to be used by all Spirent groups and divisions. It ensures consistency, quality and security of Spirent runtime components and their build processes.

The latest version of the build base image uses OpenJDK 16 / JRE

## Get

Before using Spirent dockerhub private docker registry you must login to dockerhub.
```bash
docker login
```

To pull the base image use the command below
```bash
docker pull spirent/springboot:latest
```

To see all existing base images go to [spirent-devops packages](https://gitlab.com/spirent-poc-devops?filter=image).

## Use

To use the base image you need to add the following `FROM` command at the beginning of in your Dockerfile
```bash
FROM spirent/jre:latest
```

The base image expects the working directory to be located at `/app`. So, before copying source code or binaries set the working directory as
```base
WORKDIR /app

... Do you things there...
```

## Build and Release

Build the image using powershell script.
```bash
./build.ps1
```

After the image is built publish it to github docker registry.
Before publishing login to the docker registry as described in the *Get* section.
```bash
./publish.ps1
```
