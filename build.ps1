#!/usr/bin/env pwsh

Set-StrictMode -Version latest
$ErrorActionPreference = "Stop"

# Get component data and set necessary variables
$component = Get-Content -Path "component.json" | ConvertFrom-Json

# Get buildnumber from gitlab pipeline
if ($env:GITLAB_RUN_NUMBER -ne $null) {
    $component.build = $env:GITLAB_RUN_NUMBER
    Set-Content -Path "component.json" -Value $($component | ConvertTo-Json)
}

# Set image name
$image="$($component.registry)/$($component.name):$($component.version)-$($component.build)"
$latestImage="$($component.registry)/$($component.name):latest"

# Build docker image
docker build -f "docker/Dockerfile" -t $image -t $latestImage .

